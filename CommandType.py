import enum

class CommandType(enum.Enum):
    REDIRECTION_OUTPUT = 0
    REDIRECTION_INPUT = 1
    PIPING = 2
    CHDIR = 3
    PWD = 4
    BG = 5
    BGLIST = 6
    BGKILL = 7
    BGSTOP = 8
    BGSTART = 9
    EXIT = 10
    MISCELLANEOUS = 11
    HISTORY = 12
    CHCOLOR = 13
