from CommandType import CommandType
import signal
import os

class Parser:
    @staticmethod
    def parse_command(command: str):
        command = command.strip()
        final_command = {}
        # check for pwd command
        if command == "pwd":
            final_command["type"] = CommandType.PWD
            return final_command

        # check for bglist command
        elif command == "bglist":
            final_command["type"] = CommandType.BGLIST
            return final_command

        elif command == "history":
            final_command["type"] = CommandType.HISTORY
            return final_command

        elif command == "exit":
            final_command["type"] = CommandType.EXIT
            return final_command

        # check for output redirection
        split_command = command.split(">")
        if len(split_command) == 2:
            final_command["type"] = CommandType.REDIRECTION_OUTPUT
            final_commands = list(map(lambda c: c.strip().split(), split_command))
            for final_part in final_commands:
                if not final_part:
                    final_command["error"] = "invalid output redirection format"
                    return final_command
            final_command["command"] = final_commands[0]
            final_command["output"] = final_commands[1][0]
            return final_command

        # check for input redirection
        split_command = command.split("<")
        if len(split_command) == 2:
            final_command["type"] = CommandType.REDIRECTION_INPUT
            final_commands = list(map(lambda c: c.strip().split(), split_command))
            for final_part in final_commands:
                if not final_part:
                    final_command["error"] = "invalid input redirection format"
                    return final_command
            final_command["type"] = CommandType.REDIRECTION_INPUT
            final_command["command"] = final_commands[0]
            final_command["input"] = final_commands[1][0]
            return final_command

        # check for piping
        split_command = command.split("|")
        if len(split_command) >= 2:
            final_command["type"] = CommandType.PIPING
            final_commands = list(map(lambda c: c.strip().split(), split_command))
            while [] in final_commands:
                final_commands.remove([])
            if not final_commands:
                final_command["error"] = "pipe operation with no arguments is not valid"
            final_command["command"] = final_commands
            return final_command

        split_command = command.split()

        # check for change directory
        if split_command[0] == "cd":
            final_command["type"] = CommandType.CHDIR
            if len(split_command) == 1:
                final_command["path"] = str(os.path.expanduser("~"))
            elif len(split_command) == 2:
                final_command["path"] = split_command[1]
            else:
                final_command["error"] = "too many arguments"

            return final_command

        # check for bg command
        elif split_command[0] == "bg":
            final_command["type"] = CommandType.BG
            final_command["base"] = split_command[1]
            final_command["command"] = split_command[1:]
            return final_command

        # check for bgkill or bgstop or bgstart command
        elif split_command[0] == "bgkill" or split_command[0] == "bgstop" or split_command[0] == "bgstart":
            final_command["type"] = CommandType.BGKILL if split_command[0] == "bgkill" else CommandType.BGSTOP if split_command[0] == "bgstop" else CommandType.BGSTART
            if len(split_command) == 2:
                final_command["signal"] = signal.SIGTERM if split_command[0] == "bgkill" else signal.SIGSTOP if split_command[0] == "bgstop" else signal.SIGCONT
                try:
                    final_command["index"] = int(split_command[1]) - 1
                except ValueError:
                    final_command["error"] = "can not parse second argument"

            else:
                final_command["error"] = "invalid number of arguments"

            return final_command

        elif split_command[0] == "chcolor":
            final_command["type"] = CommandType.CHCOLOR
            if len(split_command) == 1:
                final_command["error"] = "few arguments passed"
            elif len(split_command) == 2:
                final_command["color"] = split_command[1]

            else:
                final_command["error"] = "too many arguments"
            return final_command

        # other routine commands
        else:
            final_command["type"] = CommandType.MISCELLANEOUS
            final_command["base"] = split_command[0]
            final_command["command"] = split_command
            return final_command
