import re
import os
from os import _exit as kill
import sys



def extract_args_with_quotes(command):
    command_split = command.split()
    args = command_split[1:]
    base = command_split[0]

    matches = re.findall(r'(\"(.+?)\"|-?[\w][\w]*)', command)
    return matches



def handle_pipe(cmds):
    pipes = []
    pids = []
    for index in range(len(cmds)):
        pid = os.fork()
        if index == 0:
            if pid == 0:
                pipes.append(os.pipe())
                os.dup2(pipes[0][1], 1)
                os.execvp(cmds[index][0], cmds[index])
            else:
                pids.append(pid)


        elif index == len(cmds) - 1:
            if pid == 0:
                os.dup2(pipes[index - 1][0], 0)
                os.execvp(cmds[index][0], cmds[index])
                for pipe in pipes:
                    os.close(pipe[0])
                    os.close(pipe[1])
            else:
                pids.append(pid)

        else:
            if pid == 0:
                os.dup2(pipes[index - 1][0], 0)
                os.execvp(cmds[index][0], cmds[index])
                pipes.append(os.pipe())
                os.dup2(pipes[index][1], 1)
            else:
                pids.append(pid)

    for pid in pids:
        os.waitpid(pid, 0)

def pipe_handle(cmds):
    '''
    :param cmds: the commands to be executed
    :return: no return values
    :raises: Exception is raised if there are more than 3 commands fed to the function
    '''

    # duplicating file descriptors
    old_reading, old_writing = os.pipe()
    new_reading, new_writing = os.pipe()

    if len(cmds) == 2:
        test = os.fork()
        if test == 0:
            os.dup2(old_writing, 1)
            os.execvp(cmds[0][0], cmds[0])

        test2 = os.fork()
        if test2 == 0:
            os.dup2(old_reading, 0)
            os.execvp(cmds[1][0], cmds[1])

        os.close(old_reading)
        os.close(old_writing)

        os.waitpid(test, 0)
        os.waitpid(test2, 0)

    elif len(cmds) == 3:
        test = os.fork()
        if test == 0:
            os.dup2(old_writing, 1)

            os.execvp(cmds[0][0], cmds[0])

        test2 = os.fork()
        if test2 == 0:
            os.dup2(old_reading, 0)
            os.dup2(new_writing, 1)
            os.execvp(cmds[1][0], cmds[1])

        test3 = os.fork()
        if test3 == 0:
            os.dup2(new_reading, 0)
            os.execvp(cmds[2][0], cmds[2])

        os.close(old_reading)
        os.close(old_writing)

        os.close(new_reading)
        os.close(new_writing)

        os.waitpid(test, 0)
        os.waitpid(test2, 0)
        os.waitpid(test3, 0)
    else:
        raise Exception("too many arguments")


def redirection_output(command, filename):
    '''
    :param command: the command to be executed
    :param filename: the file to which the result of the command execution is written
    :return: no return values
    :raises: error occurs in case of command misformat
    '''
    outpt = os.open(filename, os.O_WRONLY | os.O_CREAT | os.O_TRUNC)

    save_out = os.dup(sys.stdout.fileno())

    os.dup2(outpt, sys.stdout.fileno())

    pid = os.fork()

    if pid == 0:
        try:
            os.execvp(command[0], command)
        except Exception:
            print("invalid command or argument")
    else:
        pid, status = os.waitpid(pid, 0)
        os.dup2(save_out, sys.stdout.fileno())
        os.close(save_out)
        os.close(outpt)


def redirection_input(word1, word2):
    '''
    :param word1: the command or file which takes as input the value produced by word2
    :param word2: the command or file which produces a result which is used as input for word1
    :return: no return values
    :raises: error occurs in case of command misformat
    '''
    outpt = os.open(word2, os.O_RDONLY)

    save_out = os.dup(sys.stdout.fileno())

    os.dup2(outpt, sys.stdin.fileno())

    pid = os.fork()

    if pid == 0:
        try:
            os.execvp(word1[0], word1)
            exit(os.EX_OK)
        except FileNotFoundError:
            print("invalid command or argument")
    else:
        pid, status = os.waitpid(-1, 0)
        os.dup2(save_out, sys.stdin.fileno())
        os.close(save_out)
        os.close(outpt)



# pipe_handle([['cat', 'test.txt'], ['grep', 'opera'], ['sort']])
# piping([['cat', 'test.txt'], ['grep', 'wxr']])
# redirection_output(["ls", "-l"], "test.txt")
# redirection_input(["sort", "-r"], "test.txt")