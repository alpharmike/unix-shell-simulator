import os
import subprocess
from Process import Process
import signal
from utils import *
import socket
import getpass
from CommandType import CommandType
from Parser import Parser
from termcolor import colored
import code
import atexit

try:
    import readline
except ImportError:
    readline = None


class Shell(code.InteractiveConsole):

    def __init__(self, locals=None, filename="<console>",
                 histfile=os.path.expanduser("~/.console-history")):
        code.InteractiveConsole.__init__(self, locals, filename)
        self.init_history(histfile)
        self.working_directory = os.getcwd()
        self.history = []
        self.background_processes = []
        self.history = []
        self.text_color = 'white'
        self.supported_colors = [
            'grey',
            'red',
            'green',
            'yellow',
            'blue',
            'magenta',
            'cyan',
            'white'
        ]

    # initializing a history file to provide auto completion and auto suggestion
    def init_history(self, histfile):
        readline.parse_and_bind("tab: complete")
        if hasattr(readline, "read_history_file"):
            try:
                readline.read_history_file(histfile)
            except FileNotFoundError:
                pass
            atexit.register(self.save_history, histfile)

    def save_history(self, histfile):
        readline.set_history_length(1000)
        readline.write_history_file(histfile)

    # taking commands from the user and executing them by calling the exec_command function
    def run(self):
        os.system("clear")
        while True:
            try:
                inp = input(
                    colored(f"{getpass.getuser()}@{socket.gethostname()}:{os.getcwd()}:Shell>", self.text_color))
            except EOFError:
                eof = True
                break
            self.exec_command(inp)

    # the core function of the class, responsible for taking a command as input as executing that command with fork and execvp system calls
    def exec_command(self, command: str):
        '''

        :param command: the user input unix command
        :return: no return values
        :raises: the raise conditions are due to commands' wrong format or non-existence of a file or directory
        :exception: FileNotFoundError
        '''

        if not command or command is None:
            return

        # the command might be invalid, but it is saved
        self.history.append(command)

        command_info = Parser.parse_command(command)
        if not command_info or command_info is None:
            return

        if command_info["type"] == CommandType.REDIRECTION_OUTPUT:
            if command_info.get("error") is not None:
                print(command_info.get("error"))
                return

            redirection_output(command_info["command"], command_info["output"])

        elif command_info["type"] == CommandType.REDIRECTION_INPUT:
            if command_info.get("error") is not None:
                print(command_info.get("error"))
                return
            try:
                redirection_input(command_info["command"], command_info["input"])
            except FileNotFoundError:
                print("No such for or directory")
            except Exception:
                print("invalid command or argument")

        elif command_info["type"] == CommandType.PIPING:
            if command_info.get("error") is not None:
                print(command_info.get("error"))
                return
            try:
                pipe_handle(command_info["command"])
            except Exception as error:
                print(error)

        elif command_info["type"] == CommandType.CHDIR:
            if command_info.get("error") is not None:
                print(command_info.get("error"))
                return
            directory = command_info["path"]
            if not os.path.exists(directory):
                print(f"cd: {directory}: No such file or directory")
                return
            os.chdir(directory)
            if directory.startswith("/"):
                self.working_directory = directory
            else:
                self.working_directory = self.working_directory + "/" + directory
            self.working_directory = os.getcwd()

        elif command_info["type"] == CommandType.PWD:
            print(os.getcwd())

        elif command_info["type"] == CommandType.HISTORY:
            self.show_history()

        elif command_info["type"] == CommandType.CHCOLOR:
            if command_info.get("error") is not None:
                print(command_info.get("error"))
                return
            elif command_info["color"] not in self.supported_colors:
                print("color not supproted")
                return

            self.text_color = command_info["color"]
            return

        elif command_info["type"] == CommandType.BG:
            pid = os.fork()
            if pid == 0:
                try:
                    os.execvp(command_info["base"], command_info["command"])
                    exit(os.EX_OK)
                except Exception as erroe:
                    print(error)
            else:
                self.add_bg_process(pid, " ".join(command_info["command"]))
                pid, status = os.waitpid(-1, os.WNOHANG)

        elif command_info["type"] == CommandType.BGLIST:
            for index in range(len(self.background_processes)):
                bg_process = self.background_processes[index]
                print(index + 1, bg_process.command, bg_process.pid, end='\n')


        elif command_info["type"] == CommandType.BGKILL or command_info["type"] == CommandType.BGSTOP or command_info[
            "type"] == CommandType.BGSTART:
            if command_info.get("error") is not None:
                print(command_info.get("error"))
                return
            self.signal_process(command_info["index"], command_info["signal"])
        elif command_info["type"] == CommandType.EXIT:
            exit(os.EX_OK)
        else:
            pid = os.fork()
            if pid == 0:
                try:
                    os.execvp(command_info["base"], command_info["command"])
                    exit(os.EX_OK)
                except Exception as error:
                    print(error)
            else:
                os.wait()

    def add_bg_process(self, pid: int, command: str):
        if command.startswith("/"):
            self.background_processes.append(Process(pid=pid, command=command))
        else:
            self.background_processes.append(Process(pid=pid, command=self.working_directory + "/" + command))

    def signal_process(self, index, sig):
        '''
        :param index: the index number of the process to catch the signal
        :param sig: the signal to be sent to a process -> KILL, STOP, or CONT
        :return: no return values
        :raises: exception is raised if the process with the given index does not exist
        '''
        if index >= len(self.background_processes):
            print("the process does not exist")
            return
        try:
            target_process = self.background_processes[index]
            os.kill(target_process.pid, sig)
            if sig == sig.SIGTERM:
                self.background_processes.pop(index)
        except:
            print("no such process")
            self.background_processes.pop(index)

    def show_history(self):
        for command in self.history:
            print(command, end='\n')
